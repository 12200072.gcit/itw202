import React from "react";
import {View,StyleSheet,Text} from 'react-native';

const MyDemo=()=>{
    return(
        <View style={styles.container}>
            <View style={styles.square1}><Text>1</Text></View>
            <View style={styles.square2}><Text>2</Text></View>
            <View style={styles.square3}><Text>3</Text></View>
            
        </View>
    )
}
export default MyDemo;
const styles=StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'row',
        marginTop:20,
        marginRight:500
     
        
        
    },
    square1:{
        flex:3,
        backgroundColor:'#ff3333',
        width:100,
        height:200,
        
    },
    square2:{
        flex:6,

      
        backgroundColor:'#0b89f9',
        width:100,
        height:200,
        
    },
    square3:{
        
        flex:1,
        backgroundColor:'green',
        width:100,
        height:200,
        
    }
})