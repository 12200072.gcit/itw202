import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, ImageBackground, SafeAreaView } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import StartGameScreen from './screens/StartGameScreen';
import GameScreen from './screens/GameScreen'
import { useState } from 'react';
import colors from './constants/Colors';
import GameOverScreen from './screens/GameOverScreen';
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';

export default function App() {
  const [userNumber, setUserNumber]=useState();
  const [gameIsOver, setGameIsOver] = useState(true);
  const [guessRound, setGuessRound] = useState(0);

  
const [fontsloaded] = useFonts({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
  })
  
  if(!fontsloaded){
   return <AppLoading/>
  }

  function pickedNumberHandler(pickerNumber){
    setUserNumber(pickerNumber)
    setGameIsOver(false);
  }
  function gameOverHandler(numberOfRounds){
    setGameIsOver(true);
    setGuessRound(numberOfRounds)
  }
  function startNewGameHandler(){
    setUserNumber(null);
    setGuessRound(0);
  }

  let screen = <StartGameScreen onPickNumber={pickedNumberHandler}/>
  if(userNumber){
    screen=<GameScreen 
          userNumber={userNumber} 
          onGameOver = {gameOverHandler} 
        />
  }
  if(gameIsOver && userNumber){
      screen = <GameOverScreen
            userNumber={userNumber}
            roundNumber={guessRound}
            onStartNewGame={startNewGameHandler}
          />
                
  }
  return (
    <>
    <StatusBar style='light'/>
    <LinearGradient colors={['#4e0329','#ddb52f']} style={styles.container}>
      <ImageBackground source={require('./assets/images/background.png')} resizeMode='cover'
        style={styles.backgroundImage}/>
      
      <SafeAreaView style={styles.container}>
      {screen}
      {/* <GameOverScreen/> */}
      </SafeAreaView>
      
    </LinearGradient >
    </>
  );
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    //backgroundColor:'green'
  },
  backgroundImage:{
    position:'absolute', 
    width:'100%', 
    height:'100%', 
    opacity:0.4 

  }
});
