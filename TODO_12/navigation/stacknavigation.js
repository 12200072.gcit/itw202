import React from "react";
import {createStackNavigation} from '@react-navigation/stack';
import { NavigationContainer } from "@react-navigation/native";
import screen1 from "../screen/screen1";
import screen2 from "../screen/screen2";



const Stack=createStackNavigation();

const Mystack =()=>{
    <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen name='screen1' component={screen1}/>
            <Stack.Screen name='screen2' component={screen2}/>
        </Stack.Navigator>
    </NavigationContainer>
    
}
export default Mystack;