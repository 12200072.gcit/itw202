import Firebase from "firebase/compat";

const firebaseConfig = {
  apiKey: "AIzaSyBbNS6x_1QvdBjhtRzm3ojfc4ML0vcHSG8",
  authDomain: "crud-3b171.firebaseapp.com",
  databaseURL: "https://crud-3b171-default-rtdb.firebaseio.com",
  projectId: "crud-3b171",
  storageBucket: "crud-3b171.appspot.com",
  messagingSenderId: "304130711579",
  appId: "1:304130711579:web:304c40e6537b5219d118be",
  measurementId: "G-SVPTYTEHTT"
};
Firebase.initializeApp(firebaseConfig);

export default Firebase;