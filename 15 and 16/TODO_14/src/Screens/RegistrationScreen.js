import React, {useState}from 'react';
import {View, StyleSheet,Text, Alert,ScrollView} from 'react-native';
import Header from '../components/Header';
import CustomInput from '../components/CustomInput';
import Button from '../components/Button';
import Background from '../components/Background';
import Logo from '../components/Logo';
import GoBack from '../components/BackBut';
import { TouchableOpacity } from 'react-native';
import { theme } from '../core/theme';
import { Formik } from 'formik';
import * as yup from 'yup';
import 'react-native-gesture-handler';
import Firebase from './firebaseConnector';

const reviewSchema = yup.object({
    name:
     yup
     .string()
     .required('Name is Required')
     .min(4),
    email:
    yup
    .string()
    .email('Please Enter Valid Email')
    .required('Email is Required'),
   
    password: 

    yup
    .string()
    .matches(/\w*[a-z]\w*/,  "Password must have a small letter")
    .matches(/\w*[A-Z]\w*/,  "Password must have a capital letter")
    .matches(/\d/, "Password must have a number")
    .matches(/[!@#$%^&*()\-_"=+{}; :,<.>]/, "Password must have a special character")
    .min(8, ({ min }) => `Password must be at least ${min} characters`)
    .required('Password is required'), 
  })

export default function RegisterScreen({navigation}){
    
    const [emailError, setEmailError] = useState(null)
    const [passwordError, setPasswordError] = useState(null)

    
    const createUser = async (values) => {
      let ref1=Firebase.database().ref().child('userdetail').push()
      ref1.set(values)
      
        Firebase.auth().createUserWithEmailAndPassword(values.email, values.password)
        .then((userCredential) => {
          // Signed in 
          var user = userCredential.user;
          var obj = {uid: user.uid, ...values}


          Alert.alert('Successful Registration!!!')
          navigation.navigate('LoginScreen',obj)      
    
        })
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log(errorMessage)
          var error = errorMessage.includes("Password")
          if(error){
            setPasswordError(errorMessage)        
            setEmailError(null)
            console.log(error)
          } else{
            setPasswordError(null)        
            setEmailError(errorMessage)
            console.log(error)
          }
          // ..
        });
      }
    
       
      
    
      
    return (
         <ScrollView>
           <View style={{marginLeft:20,width:'90%'}}>
             
            <GoBack goBack={navigation.goBack}/>
            <View style={{marginLeft:100,marginTop:60}}>
            <Logo/>
            <Header>Create Account</Header>
            </View>
          
            <View style={styles.container}>
            <Formik
        validationSchema = {reviewSchema}
        initialValues={{
          name: '',
          email: '',
          password: '',
          
        }}
        onSubmit = {(values,actions) => {
        // console.log(values)
        actions.resetForm()
        createUser(values)
        // console.log('My Id -->', uid)
        // navigation.navigate('UploadPicture')  
        }}
     

      >
      {(props)=>(
        <View >

          <CustomInput
            label='Name'
            onChangeText = {props.handleChange('name')}
            value = {props.values.name}
           
          />
          <Text style={styles.errorText}>{props.touched.name && props.errors.name}</Text>

          <CustomInput
            label='Email'
            onChangeText = {props.handleChange('email')}
            value = {props.values.email}
            onBlur = {props.handleBlur('email')}
            
          />
          {emailError ? 
            <Text style={styles.errorText}>{emailError}</Text>
            :
            null
          }
          <Text style={styles.errorText}>{props.touched.email && props.errors.email}</Text>

         
          <CustomInput
            label='Password'
            onChangeText = {props.handleChange('password')}
            value = {props.values.password}
            onBlur = {props.handleBlur('password')}
          />
          {passwordError ? 
            <Text style={styles.errorText}>{passwordError}</Text>
            :
            null
          }
          <Text style={styles.errorText}>{props.touched.password && props.errors.password}</Text>

          <Button 
            mode='contained'
            onPress={props.handleSubmit}
         
          >Submit</Button>
           <View style={styles.row}>
                <Text>Already have an account?</Text>
                <TouchableOpacity onPress={()=>navigation.replace("LoginScreen")}>
                    <Text style={styles.link}>  Login</Text>
                </TouchableOpacity>
            </View>
        
      </View>
    )}
    
    </Formik>
    </View>
    </View>
    </ScrollView>


    );
}

const styles = StyleSheet.create({
    container: {

      alignSelf:'center',
      backgroundColor: '#fff',
      width:'100%',
      marginTop:40      
      
    },
    row:{
        flexDirection:'row',
        marginTop:4
    },
  
    errorText: {
        color: 'red',
      },
      link:{
        fontWeight:'bold',
        color:theme.colors.primary
    }

  });