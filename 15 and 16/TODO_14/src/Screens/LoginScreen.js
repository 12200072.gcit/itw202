import React, {useState}from 'react';
import {View, StyleSheet, Text, TouchableOpacity,ScrollView} from 'react-native';
import Header from '../components/Header';
import CustomInput from '../components/CustomInput';
import Button from '../components/Button';
import Background from '../components/Background';
import Logo from '../components/Logo';
import { emailValidator } from '../core/Helpers/emailvalid';
import { passwordValidator } from '../core/Helpers/passwordvalid';
import GoBack from '../components/BackBut';
import { theme } from '../core/theme'; 
import { Formik } from 'formik';
import * as yup from 'yup';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Firebase from './firebaseConnector';

const reviewSchema=yup.object({
    email:
    yup
    .string()
    .email('Please enter vaild email')
    .required('Email is required'),
    password:
    yup
    .string()
    .required('Password is required'),


})

export default function LoginScreen({navigation}){
    const [emailError, setEmailError] = useState(null)
    const [passwordError, setPasswordError] = useState(null)
    // const[loading,setLoading]=useState();

    const storeData = async (value) => {
        try {
          await AsyncStorage.setItem('user', value)
          navigation.navigate('HomeScreen')
          console.log('gs')
        } catch (e) {
          // saving error
        }
      }
    
      const LoginUser = async (values) => {
     
        Firebase.auth().signInWithEmailAndPassword(values.email, values.password)
        .then((userCredential) => {
          var user = userCredential.user;
          console.log(user.uid)
          storeData(user.uid)
          // ...
        })
    
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
          var error = errorMessage.includes("password")
            console.log(errorMessage)
          if(error){
            setPasswordError('The Password is Invalid')        
            setEmailError(null)
            console.log(error)
          } else{
            setEmailError(`${values.email} is not registered email`)        
            setPasswordError(null)        
            console.log(error)
          }
          // ..
        });
      //   setLoading(true)
      //   const response=await LoginUser({
      //     emailError:emailError.values,
      //     passwordError:passwordError.values,

      //   })
      //   if(response.error){
      //     alert(response.error);
      //   }
      //   else{
      //     alert(response.user.DisplayemailError);
      //   }
      //   setLoading(false);
      }
    
   

  
    return (
          <ScrollView>
          <View style={{width:'90%',marginLeft:15}}>
            <GoBack goBack={navigation.goBack}/>
            <View style={{marginLeft:100,marginTop:30}}>
            <Logo/>
            <Header>Welcome</Header>
            </View>
         
            <Formik
        validationSchema = {reviewSchema}
        initialValues={{
          email: '',
          password: '',
        }}
        onSubmit = {(values, actions) => {
        actions.resetForm()
        LoginUser(values)
        // console.log(values)
        }}
      >
      {(props)=>(
        <View style={styles.FormContainer}>
        
          <CustomInput
            onChangeText = {props.handleChange('email')}
            value = {props.values.email}          
            label='Email'
          />
          {emailError ? 
            <Text style={styles.errorText}>{emailError}</Text>
            :
            null
          }
          <Text style={styles.errorText}>{props.touched.email && props.errors.email}</Text>
         

          <CustomInput
            onChangeText = {props.handleChange('password')}
            value = {props.values.password}
            label='Password'
            style={styles.input}
          />
          {passwordError ? 
            <Text style={styles.errorText}>{passwordError}</Text>
            :
            null
          }
          <Text style={styles.errorText}>{props.touched.password && props.errors.password}</Text>
          <View style={styles.forgotPassword}>
                <TouchableOpacity 
                    onPress={()=>navigation.navigate("ResetPasswordScreen")}>
                    <Text style={styles.forgot}> Forgot Your password? </Text>
                </TouchableOpacity>
            </View>

          <Button 
            // loading={loading}
            mode='contained'
            onPress={props.handleSubmit}
          >Login</Button>
          <View
            style={{marginTop: 10}}>
              <View style={styles.row}>
                    <Text>Don't have an account? </Text>
                    <TouchableOpacity onPress={()=>navigation.replace("RegisterScreen")}>
                        <Text style={styles.link}>  Sign up</Text>
                    </TouchableOpacity>
                </View>

            
          </View>
        </View>
        )}
      
      </Formik>
            
      </View>
      </ScrollView> 
        
    )
}

const styles = StyleSheet.create({
    row:{
        flexDirection:'row',
        marginTop:4
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary
    },
    forgotPassword:{
        width:'100%',
        alignItems:'flex-end',
        marginBottom:24,
    },
    forgot:{
        fontSize:13,
        color:theme.colors.secondary,
    },
    errorText: {
        color: 'red',
      },
      FormContainer:{
        marginTop:80

      },
      input:{
        width:'100%'
      }
  });