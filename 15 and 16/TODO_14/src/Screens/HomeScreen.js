import { View, Text, Button} from 'react-native'
import React from 'react'
import Header from '../components/Header'
import Background from '../components/Background'

export default function HomeScreen() {
  return (
    <Background>
        <Header> Hi! Welcome to Home Screen.</Header>
    </Background>
  )
}