import React,{useState} from 'react';
import { View,Text, TextInput,StyleSheet,Alert} from 'react-native';

export default function TODO_10() {
    const [text, setText] = useState();
    const[showText,setShowText]=useState(true);
    const handleText=(text)=>{
        setText("Hi"+" "+text)
        setShowText(true)
    }   
           
        return(

            <View style={styles.container}>
              {showText &&<Text style={{marginBottom:100}}>{text}</Text>}
              
                <Text>what is your name?</Text>
                <TextInput secureTextEntry  style={{borderWidth:1,width:100}}
                onChangeText={handleText}
                />
              
            </View>
        )
}
const styles=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',

        
    }
})
