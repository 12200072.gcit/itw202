import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {add,multiply} from './components/nameExport';
import {divide} from './components/defaultExport';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
      <Text>Result of Addition:{add(5,6)}</Text>
      <Text>Result of multiplication:{multiply(5,8)}</Text>
      <Text>Result of division:{divide(10,2)}</Text>
      <StatusBar style='auto'/>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text:{
    fontSize:50,
    fontWeight:'bold',
    color:'red',

  }
});
