import React, { useState } from 'react';
import { View, Text, Button } from 'react-native';


const Counter = () => {
    const [count, setCount] = useState(1);
    const[showText,setShowText]=useState("The button not yet press");
    const[disable,setDisable]=useState(false);

 
    const handleText=()=>{

        if(count<=3){
            setShowText("The button has been pressed"+" "+count+" "+"times")
            setCount(count+1)

            if(count==3){
                setDisable(true);
            }
           
        }
       
    }
        return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <View style={{marginTop:'110%'}}>
            <Text>{showText}</Text>   
            </View>  
         

            <View style={{width:'100%',maxHeight:100}}>
            <Button 
             title="Press ME"
             onPress={handleText}
             disabled={disable}

           
             
                />
           </View>
         
       </View>
   );
}
export default Counter;