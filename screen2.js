import { View, Text,Button } from 'react-native'
import React from 'react'

export default function Screen2({navigation}) {
  return (
    <View  style={{flex:1,alignItems:'center',justifyContent:'center'}}>
      <Text>screen2</Text>
      {/* <Button title='go back' onPress={()=>{navigation.goBack()}}/> */}
      <Button title='go to the third screen ' onPress={()=>{navigation.replace('Screen3')}}/>


    </View>
  )
}