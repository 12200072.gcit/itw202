import { StatusBar } from 'expo-status-bar';
import { Image, StyleSheet, Text, View } from 'react-native';
import {Provider } from 'react-native-paper';
import { theme } from './src/core/theme';
import Button from './src/components/Button';
import CustomInput from './src/components/CustomInput';
import Header from './src/components/Header';
import Paragraph from './src/components/paragraph';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import 'react-native-gesture-handler'

import {StartScreen,
        LoginScreen,
        RegisterScreen,
        ResetPasswordScreen, 
        HomeScreen,
        ProfileScreen
} from './src/Screens/Index';
import Mystack from './navigation/Mystack';


const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  return (
    // <Provider theme={theme}>
    //   {/* <View style={styles.container}>
    //     <StatusBar style="auto" />
    //     <Header>Open up App</Header>
    //     <Paragraph>
    //       To sum up, we have added a design system to our app with react-native-paper. 
    //       Now we have more components to build our application with.
    //     </Paragraph>
    //     <Button mode='contained'>Click Me</Button>
    //     <CustomInput label="Email"/>
    //   </View> */}
    // </Provider>
    <Provider>
      <NavigationContainer>
        <Mystack/>
      </NavigationContainer>
    </Provider>
      
      
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
