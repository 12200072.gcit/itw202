import React from "react";
import {View,Text, StyleSheet} from 'react-native';


const MyComponent=()=>{
    const name="Pema Norbu";

    return(
        <View style={styles.container}>
        <Text style={styles.text}>
            Getting started with React Native!
        
        </Text>
        <Text style={styles.text1}>
            My name is {name}
        </Text>
        </View>
        

    )
}
const styles=StyleSheet.create({
    text:{
        fontSize:45,

    },
    text1:{
        fontSize:20,
    }
})
export default MyComponent;