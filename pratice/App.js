import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Button, Alert, Platform, ActivityIndicator } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { useRoute } from '@react-navigation/native';
import * as ImagePicker from 'expo-image-picker';


export default function App({navigation}) {


  return (
    <View style={styles.container}>
     
      {/* -------- Logo Container -------------- */}
      <View style={styles.LogoContainer}>
       
      </View>

      {/* -------- Text Container --------- */}
      <View style={{marginBottom: 20}}>
        <Text
          style={{color: '#d60505', fontSize: 26, fontWeight: 'bold', marginLeft: 25}}>
        </Text>
        
      </View>

       
      </View>


  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  LogoContainer:{
    alignItems: 'center',
    marginTop: 40,
    marginBottom: 10
  },
  LogoImage: {
    height: 100,
    width: 100,
  },
  Image: {
    resizeMode: 'contain',
    width: 150,
    height: 150,
    borderRadius: 400,
    marginVertical: 40,
 },
 loading: {
  position: 'absolute',
  zIndex: 1,
  backgroundColor: 'white',
  opacity: 0.8,
  height: '100%',
  width: '100%'
}
});
