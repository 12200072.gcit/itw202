import React from 'react';
import {Text,View} from 'react-native';

const Hello=(props)=>{
    return(
        <View>
            <Text>hello{props.name}</Text>
        </View>
    );
};
const Greeting=()=>{
    return(
        <View>
            <Hello name="pema"/>
            <Hello name="norbu"/>
            <Hello name="chimi"/> 
            </View>
    )
}
export default Greeting;