import React,{useState} from "react";
import {TextInput, SafeAreaView,StyleSheet} from 'react-native';

const Demo4=()=>{
    const[text,onChangeText]=useState("");
    const[number,onChangeNumber]=useState(null);
    
    return(
        <SafeAreaView>
           
            <TextInput onChangeText={onChangeText} value={text}  style={styles.input}/>
            <TextInput onChangeText={onChangeNumber} value={number}  style={styles.input}/>
        </SafeAreaView>
    )
}
const styles=StyleSheet.create({
    input:{
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,

    }
})
export default Demo4;