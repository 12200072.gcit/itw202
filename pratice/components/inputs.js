import { View, Text,Button } from 'react-native'
import React, {useState}from 'react'
import * as ImagePicker from 'expo-image-picker';
import { Image } from 'react-native';


export default function App() {
  const[image,setImage]=useState(null);
  
 const handleImage=async()=>{
   const result=await ImagePicker.launchImageLibraryAsync({

   });

  
  console.log(result);
  if(!result.cancelled){
    setImage(result.uri);
  };

 }

  return (
    <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
      {image && <Image source={{ uri: image }} style={{ width: 200, height: 200,borderRadius:180 }}/>}

      <Button title='Choose Photo' onPress={handleImage}/>
    </View>
  )
}